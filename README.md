<div align="center">
<h1>
Guess the Number!
</h1>
</div>
Guess the Number is a small JavaScript application that showcases fundamental web development concepts like click event handling, DOM manipulation and the use of ternary operator to maintain concise and efficient code while adhering to the DRY principle by encapsulating repetitive functionality with reusable functions, ensuring code reusability, maintainabilty and readability.

The goal of the game is to guess a secret number between 1 and 15. Your starting score is 15, but with each incorrect guess, it drops by one. Your mission is to guess the number before your score hits zero. If you succeed, your current score becomes your highscore.
