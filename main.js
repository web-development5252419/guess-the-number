'use strict';

let number = Math.trunc(Math.random() * 15) + 1;
let score = 15;
let highScore = 0;

const displayNumberMessage = function (message) {
    document.querySelector('.number-message').textContent = message;
}

const displayUserScore = function (userScore) {
    document.querySelector('.score').textContent = userScore;
}

const displayUserHighScore = function (userHighScore) {
    document.querySelector('.high-score').textContent = userHighScore;
}

const displayCorrectNumber = function (correctNumber) {
    document.querySelector('.correct-number').textContent = correctNumber;
}

const setBackgroundColor = function (backgroundColor) {
    document.body.style.backgroundColor = backgroundColor;
}

displayUserHighScore(highScore);
displayUserScore(score);

document.querySelector('.check-number').addEventListener('click', function () {
    const userInput = Number(document.querySelector('.input-user').value);

    if (!userInput || userInput > 15) {
        displayUserScore(score);
        displayNumberMessage('Type a number between 1 and 15');
        setBackgroundColor('#FF9526');
    } else if (userInput === number) {
        displayUserScore(score);
        displayCorrectNumber(number);
        displayNumberMessage('You guessed the number !');
        setBackgroundColor('#4FAF44');

        if (score > highScore) {
            highScore = score;
            displayUserHighScore(highScore);
        }
    } else if (userInput !== number) {
        if (score > 1) {
            score--;
            displayUserScore(score);
            displayNumberMessage(userInput > number ? 'Too high !' : 'Too low !');
            setBackgroundColor('#FF9526');
        } else {
            score = 0;
            displayUserScore(0);
            displayNumberMessage('YOU LOST!');
            setBackgroundColor('#EF4423');
        }
    }
});

document.querySelector('.play-again').addEventListener('click', function () {
    score = 15;
    number = Math.trunc(Math.random() * 15) + 1;
    document.querySelector('.input-user').value = '';
    displayUserScore(score);
    displayNumberMessage('Start guessing');
    displayCorrectNumber('?');
    setBackgroundColor('#2A3492');
});